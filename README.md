# Samba container

The container is build and pushed to this [Quay.io repo](https://quay.io/repository/juliu_s/samba) with a GitLab pipeline.

[[_TOC_]]

# Get the container

## Build container yourself

```console
git clone https://gitlab.com/juliu-s/container-samba
cd container-samba
podman build . -t foo/samba
```

## Pull version from Quay.io

```console
podman pull quay.io/juliu_s/samba:v1.0.0
```

# Usage

When the container starts `/usr/local/bin/run-samba` is executed that first executes `/usr/local/bin/setup-samba`. The setup script generates `/etc/samba/smb.conf` based on defaults and environment variabels.

To create users the environment variabele must have a prefix of `SAMBA_USER_*`. The password **can't contain ":" or "="** characters, if you want that, you can use the password file option. Example:

```bash
SAMBA_USER_1="user1:password1"
SAMBA_USER_2="user2:path_i_contianer_to_password_file"
```

To create a share the environment variabele must have a prefix of `SAMBA_SHARE_*`. The following options are supported:

| Samba share option | Required / optional |
| ------------------ | ------------------- |
| name               | Required            |
| path               | Required            |
| browsable          | Optional            |
| read only          | Optional            |
| guest ok           | Optional            |
| valid users        | Optional            |
| write list         | Optional            |
| admin users        | Optional            |
| comment            | Optional            |
| timemachine[1]     | Optional            |

- [1] When setting timemachine to for example yes, extra share config is appended for Mac backups, [click here for more info](https://wiki.samba.org/index.php/Configure_Samba_to_Work_Better_with_Mac_OS_X)

For example:

```bash
SAMBA_SHARE_1="name=test_for_user_1:path=/data/test_for_user_1:browsable=yes:readonly=no:guestok=no:validusers=user1:comment=share for user 1"
SAMBA_SHARE_2="name=test_for_user2:path=/data/foobar:browsable=yes:readonly=no:guestok=no:validusers=user2:adminusers=user1"
SAMBA_SHARE_3="name=test_for_user3:path=/data/user3_data:browsable=no:readonly=no:guestok=no:validusers=user2:writelist=user3"
```

See the `.gitlab-ci.yml` for another example.

## Self build version

```console
sudo podman run -d \
  --name samba \
  -p 445:445 \
  -e SAMBA_USER_1="user1:password" \
  -e SAMBA_SHARE_1="name=test_for_user_1:path=/data/test_for_user_1:browsable=yes:readonly=no:guestok=no:validusers=user1:writelist=user1:comment=share for user 1" \
  foo/samba
```

## Quay.io version

```shell-session
sudo podman run -d \
  --name samba \
  -p 445:445 \
  -e SAMBA_USER_1="user1:password" \
  -e SAMBA_SHARE_1="name=test_for_user_1:path=/data/test_for_user_1:browsable=yes:readonly=no:guestok=no:validusers=user1:writelist=user1:comment=share for user 1" \
  quay.io/juliu_s/samba:v1.0.0
```

# Default env vars

| Environment variable      | Default                                             |
| ------------------------- | --------------------------------------------------- |
| TZ                        | Europ/Amsterdam                                     |
| SAMBA_WORKGROUP           | WORKGROUP                                           |
| SAMBA_SERVER_STRING       | Samba Server in a container                         |
| SAMBA_SERVER_MIN_PROTOCOL | SMB3                                                |
| SAMBA_SERVER_MAX_PROTOCOL | SMB3                                                |
| SAMBA_SERVER_ENCRYPT      | required                                            |
| SAMBA_SERVER_SIGNING      | mandatory                                           |
| SAMBA_CLIENT_MIN_PROTOCOL | SMB3                                                |
| SAMBA_CLIENT_MAX_PROTOCOL | SMB3                                                |
| SAMBA_LOG_LEVEL           | 0                                                   |
| SAMBA_FOLLOW_SYMLINKS     | yes                                                 |
| SAMBA_WIDE_LINKS          | yes                                                 |
| SAMBA_HOSTS_ALLOW         | 127.0.0.0/8 10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 |
