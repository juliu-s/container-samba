# [1.3.0](https://gitlab.com/juliu-s/container-samba/compare/v1.2.0...v1.3.0) (2025-01-31)


### Features

* Update Samba config ([964ac78](https://gitlab.com/juliu-s/container-samba/commit/964ac78b104c6cbbb8f08d1c3a6d3be2967540bb))

# [1.2.0](https://gitlab.com/juliu-s/container-samba/compare/v1.1.0...v1.2.0) (2024-11-13)


### Features

* **deps:** update registry.access.redhat.com/ubi8/nodejs-20 docker digest to 03d0ab4 ([65ba886](https://gitlab.com/juliu-s/container-samba/commit/65ba88644edf689b3756d6857fe3be133513e8d4))

# [1.1.0](https://gitlab.com/juliu-s/container-samba/compare/v1.0.0...v1.1.0) (2024-11-06)


### Features

* **deps:** update quay.io/podman/stable docker tag to v5.2.5 ([a336189](https://gitlab.com/juliu-s/container-samba/commit/a3361895508b8157c2a8a2105d64b55e512fc84d))

# 1.0.0 (2024-10-16)


### Features

* Trigger semantic-release with empty commit ([4a8cfa3](https://gitlab.com/juliu-s/container-samba/commit/4a8cfa37c555e11432acdf6e3e50f0c77988f42b))
